FILE     = onion
PDFS     = $(FILE).pdf
INCLUDES = figures.tex discussion.tex results.tex mm.tex intro.tex abstract.tex
FIGURES  = # fig01.pdf fig02.pdf fig03.pdf
all:  $(PDFS) 

.SUFFIXES: .tex .pdf

$(FILE).pdf: $(FILE).tex Makefile $(FILE).bib $(FIGURES)

# a general rule how to produce .pdf from .tex
.tex.pdf:
	pdflatex $*
	bibtex   $*
	pdflatex $*
	pdflatex $*

clean:
	rm -rf *.log *.aux *.bbl *.blg $(PDFS) *.toc *~ *.out *.loc
